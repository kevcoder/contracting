Vue.component('kctr-client', {
    props: {
        client: Object,
        onSaveEventName: { type: String, default: 'on-save' }
    },
    data: function () {
        return {
            c: this.client
        }
    },
    /*html*/
    template: `
    <section>
        <div class="row" >
            <input type="text" placeholder="street #" v-model="c.address.street" /> 
            <input type="text" placeholder="city" v-model="c.address.city"/> 
            <select v-model="c.address.state">
                <option value="">State</option>
                <option value="AK">Alaska</option>
                <option value="AL">Alabama</option>
                <option value="AR">Arkansas</option>
                <option value="AZ">Arizona</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DC">District of Columbia</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="IA">Iowa</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="MA">Massachusetts</option>
                <option value="MD">Maryland</option>
                <option value="ME">Maine</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MO">Missouri</option>
                <option value="MS">Mississippi</option>
                <option value="MT">Montana</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="NE">Nebraska</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NV">Nevada</option>
                <option value="NY">New York</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="PR">Puerto Rico</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VA">Virginia</option>
                <option value="VT">Vermont</option>
                <option value="WA">Washington</option>
                <option value="WI">Wisconsin</option>
                <option value="WV">West Virginia</option>
                <option value="WY">Wyoming</option>
            </select>
            <input type="text" placeholder="zip code" v-model="c.address.zip" /> 
        </div>
        <div class="row">
            <input type="text" placeholder="$ Rate" v-model="c.rate" /> <span> per </span> 
            <input type="radio" name="rateType" id="rateTypeHr" value="hr" v-model="c.rateType"> <label for="rateTypeHr">Hour </label>
            <input type="radio" name="rateType" id="rateTypeFlat" value="flat" v-model="c.rateType"> <label for="rateTypeFlat">Flate Rate </label>
        </div>

        <div class="row">
            <kctr-grid caption="Contacts" :columns="[{'First':'first'},{'Last':'last'},{'Title':'title'}]" :data="c.contacts"  />
        </div>

        <div class="row">
            <kctr-grid caption="Work History" :columns="[{'Type':'type'},{'Name':'name'},{'Signed':'dtSigned'},{Misc:'misc'}]" :data="c.documents"  />
        </div>

        <div class="row">
            <div class="col-lg-12">
                <button class="" v-on:click="$emit(onSaveEventName,c)">Save
                </button>
            </div>
        </div>
    </section>
    `
});

