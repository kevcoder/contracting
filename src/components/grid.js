Vue.component('kctr-grid', {
    props: {
        data: { type: Array, default: [] },
        columns: { type: Array, default: [] },
        caption: String,
        onEditEventName: { type: String, default: 'on-edit' }
    },
    computed: {
        headers: function () {
            if ((this.columns || []).length === 0) {
                return Object.keys(this.data[0]).map(k => { k: k });
            }
            else {
                return this.columns;
            }
        }

    },
    template: `
    <table class="table table-striped table-sm">
    <caption> {{ caption }} </caption>
    <thead >
        <tr>
            <th> # </th>
            <th scope="col" v-for="h in headers">{{ Object.keys(h)[0] }}</th>            
        </tr>
    </thead>
    <tbody>
        <tr scope="row" v-for="d in data">
            <td> <a href="#" v-on:click="$emit(onEditEventName, d)"> edit </a> </td>
            <td v-for="k in headers">{{ d[k[Object.keys(k)[0]]] }}</td>
        </tr>
    </tbody>
    </table>
    `
})