Vue.component('kctr-nav-menu', {
    props: {
        id: { type: String, default: '' },
        linkText: { type: String, default: '' },
        onClickEventName: { type: String, default: 'on-clicked' }
    },
    template: `<a href="#" class="list-group-item" v-on:click="$emit(onClickEventName,id)">{{linkText}}</a>`
});