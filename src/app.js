let clients = [{
    name: '',
    address: {
        street: '', city: '', state: '', zip: ''
    },
    rate: 0.00,
    rateType: '',
    notes: [''],
    contacts: [
        { first: '', last: '', title: '', meansOfContact: [{ contactMechanism: '', contactData: '', isDefault: true }] }
    ],
    documents: [
        { type: '', name: '', dtSigned: null, misc: '', documentId: null }
    ],
    workHistory: [
        {
            dtSigned: null, dtApproved: null, name: '', documentId: null, invoice: '', dtPaid: null, amountBilled: 0.00, amountPaid: 0.00
            , hours: [{ startTime: null, endTime: null, notes: '' }]
        }
    ]
}];


var app = new Vue({
    el: '#app',
    data: { clients: clients, selectedClient: null },
    computed: {
        listOfClients: function () {
            return this.clients.map(c => {
                return { name: c.name, id: c.id };
            });
        }
    },
    methods: {
        onClientSelected: function (id) {
            if (id) {
                this.selectedClient = this.clients.find(c => c.id === id);
                console.info('selectedClient', this.selectedClient);
            }
        },
        onClientSaved:function(savedClient){
            if(savedClient){
                console.info('savedClient',savedClient);
            }
        }
    },
    mounted: function () {
        this.clients = [];
        fetch('http://localhost:3000/clients')
            .then(data => {
                if (!data.ok) {
                    throw `error retrieving clients ${data.status} : ${data.statusText}`;
                }
                return data.json()
            })
            .then(resp => this.clients = resp)
            .catch(err => console.error(err));
    }
})